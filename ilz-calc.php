<!--
ilz-calc
Copyright (C) 2020  Jonas Hucklenbroich <jonas@hucklenbroich.tk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
/*
  $base = [
      .. => "count",
      .. => "value",
  ];
  $var = [
      .. => "value",
      .. => "max",
  ];
  $target = number;
*/
function genVariant($base, $var, $target) {
    $remaining = $target - ($base['count'] * $base['value']);
    $varcount = ceil($remaining / $var['value']);
    $overflow = (($varcount * $var['value']) + ($base['count'] * $base['value'])) - $target;
    if ($varcount > $var['max']) {
        return false;
    } else {
        return array(
            'base' => $base['count'],
           'var' => $varcount,
            'overflow' => $overflow,
        );
    }
}

function genAll($base, $var, $target) {
    $variants = array();
    for ($basecnt = 0; $basecnt <= $base['max']; $basecnt++) {
        $result = genVariant(
            array(
                'count' => $basecnt,
                'value' => $base['value'],
            ),
            array(
                'value' => $var['value'],
                'max' => $var['max'],
            ),
            $target);

         if ($result != false) {
             $variants[] = $result;
         }
     }
     return $variants;
 }
 
            
        
        
    

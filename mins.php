<!--
     ilz-calc
     Copyright (C) 2020  Jonas Hucklenbroich <jonas@hucklenbroich.tk>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
$entered = isset($courses);
$lang = mkbool($lang);
$repcourse = mkbool($repcourse);

function calcMins($courses, $lang, $repcourse) {
    return ($courses * 45) + ($lang ? 15 : 0) + ($repcourse ? 30 : 0);
}
function mkbool($value) {
    if (isset($value)) {
	if ($value == "on") {
	    return true;
	} else {
	    return false;
	}
    } else {
	return false;
    }
}
?>
<h3>Benötigte Minuten berechnen</h3>
<p class="intro">Zähle zuerst wie viele Kurse du ohne die zweite Fremdsprache hast. Finde dann heraus ob du eine zweite Fremdsprache (Spanisch/Französisch) hast und gib dies hierunten ein.</p>
<br>
<?php if (!$entered): ?>
<form action="index.php" method="get">
    <input type="hidden" name="mode" value="min"/>
    <p>Wie viele Kurse hast du? (ohne 2. Fremdsprache)<br><input placeholder="z.B. 11" type="number" min="0" max="30" name="courses" required/></p>
    <p>Hast du eine zweite Fremdsprache?<br><input type="checkbox" name="lang"/></p>
    <p>Hast du einen Vertiefungskurs?<br><input type="checkbox" name="repcourse"/></p>
    <input type="submit" />
</form>
<br>
<button onclick="window.history.back()">Zurück</button>
<?php else: ?>
<h2><?php 
$mins = calcMins($courses, $lang, $repcourse);
echo $mins;
?> Minuten! (<?php echo $mins / 60 ?> Stunden)</h2>
<button onclick="window.history.go(-2)">Merke dir diesen Wert und gib ihn hier ein.</button>
<?php endif ?>

<!--
ilz-calc
Copyright (C) 2020  Jonas Hucklenbroich <jonas@hucklenbroich.tk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
  $str = parse_str($_SERVER['QUERY_STRING']);
parse_url($str);

?>

<html>
    <head>
	<title>ILZ Rechner</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-deep-orange.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<h1>ILZ Rechner</h1>
	<a href="https://gitlab.com/johuck/ilz-calc">Freie/Open Source Software unter der GPLv3</a>
	<p>(C) Copyright 2020 Jonas Hucklenbroich</p>
	<?php
	switch($mode) {
	    case "min":
		include "mins.php";
		break;
	    default:
		include "hour-calc.php";
	}
	?>
        
       <!-- <div id="bottom-bar">
            <div id="foss">
                <p>
                    Der ILZ rechner ist Freie/Open Source Software unter der <a href="http://www.gnu.de/documents/gpl.de.html">GPLv3</a>.
                    Du kannst den Quellcode auf <a href="https://gitlab.com/johuck/ilz-calc">GitLab herunterladen</a>.
                </p>
            </div>
            <p>(C) Copyright 2020 Jonas Hucklenbroich</p></div>-->
	
	<style>
         * {
             font-family: "Arial";
         }
         body {
             margin:0;
             padding:0;
             width: 100%;
             text-align: center;
             padding-left:25px;
             background-color: lightgrey;
             overflow: hidden;
             min-height: 100%;
         }
   
	 input {
	     min-width: 100px;
	 }
       
         #foss {
	     margin-bottom:40px;
	 }
	 
	 #bottom-bar {
	    /* position: absolute;
	     width: 100%;
	     height:110px;
	     border-top-left-radius: 15px;
	     border-top-right-radius: 15px;
	     bottom: 0;
	     left:0;
	     text-align: center;
	     background-color: darkgrey;*/
	     clear: both;
	    position: relative;
	    height: 200px;
	    margin-top: -200px;
	     
	 }
	</style>
    </body>
</html>

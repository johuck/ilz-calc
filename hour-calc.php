<!--
     ilz-calc
     Copyright (C) 2020  Jonas Hucklenbroich <jonas@hucklenbroich.tk>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
$entered = isset($sixty) && isset($ninety) && isset($mins);
$variations = array();

function displayVars($six, $nine, $target) {
    global $variations;
    include_once "ilz-calc.php";
    $variations = genAll(
        $base = [
            'value' => 60,
            'max' => $six,
        ],
        $var = [
            'value' => 90,
            'max' => $nine,
        ],
        $target);
    foreach ($variations as $i => $variation) {
?>
    <tr>
	<td class="num"><?= $i+1 ?></td>
	<td class="sixty"><?= $variation['base']?></td>
	<td class="ninety"><?= $variation['var']?></td>
	<td class="overflow"><?= $variation['overflow']?>min</td>
    </tr>
<?php
}}
?>

<!-- <a href="ilz-rechner-doc.html"><button>Anleitung</button></a> -->

<br>
<?php if (!$entered): ?>
    <h3>Schritte zum berechnen deiner ILZ stunden</h3>
    <p class="intro">Zuerst zählst du wie viele 60 Minuten lange ILZ Blöcke du hast, dann wie viele 90 Minuten ILZ Blöcke und zuletzt musst du noch ausrechnen wie viele Minuten du insgesamt (pro Woche) brauchst. Solltest du noch nicht wissen wie viele Minuten du brauchst kannst du das <a href="index.php?mode=min">hier berechnen--></a></p>
    <form action="index.php" method="get">
	<p>Anzahl deiner <b>60 minuten</b> ILZ Blöcke:<br> <input placeholder="z.B. 6" type="number" name="sixty" required/></p>
	<p>Anzahl deiner <b>90 minuten</b> ILZ Blöcke:<br> <input placeholder="z.B. 3" type="number" name="ninety" required/></p>
	<p>Mindestanzahl an ILZ minuten:<br> <input placeholder="z.B. 510" type="number" name="mins" required/> <a href="index.php?mode=min">Berechnen --></a></p>
	<input type="submit" />
    </form>
<?php else: ?>
    <h3>Ergebniss</h3>
    <p>Hier siehst du nun zwei Tabellen. <br>Die erste zeigt dir wie viele blöcke du bei der jeweiligen Kombination da sein musst und die zweite wie viele du freinehmen kannst.
	<br>
    Der Rest gibt an wie viele minuten du "zu viel" da wärst. </p>
    <br>
    <h4>So viele blöcke musst du da sein</h4>
    <table>
	<tr>
	    <th class="num">Variante</th>
	    <th class="sixty">60min Blöcke</th>
	    <th class="ninety">90min Blöcke</th>
	    <th class="overflow">Rest</th>
	</tr>
	<?php displayVars($sixty, $ninety, $mins) ?>
    </table>
    <br>
    <h4>So viele Blöcke kanst du früher gehen</h4>
    <table>
	<tr>
	    <th class="num">Variante</th>
	    <th class="sixty">60min Blöcke</th>
	    <th class="ninety">90min Blöcke</th>
	    <th class="overflow">Rest</th>
	</tr>
	<?php foreach ($variations as $i => $var): ?>
	    <tr>
		<td class="num"><?= $i+1?></td>
		<td class="sixty"><?= $sixty - $var['base'] ?></td>
		<td class="ninety"><?= $ninety - $var['var'] ?></td>
		<td class="overflow"><?= $var['overflow'] ?>min</td>
	    </tr>
	<?php endforeach ?>
    </table>
<?php endif ?>

<style>
 table .sixty {
     background-color: lightblue;
 } table .ninety {
     background-color: lightgreen;
 }
 table .overflow {
     background-color: lightyellow
 }
 table, th, td {
     border: 1px solid black;
     border-collapse: collapse;
 }
 th, td {
     padding: 15px;
 }
 table {
     border-spacing: 5px;
 }
</style>

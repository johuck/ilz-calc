# ILZ Calculator
- [deutsch/german](README.de.md)

The ILZ Calculator calculates the possible combinations of 90 and 60 minute ILZ blocks to reach the minimum time. It also implements a option to calculate that time.


## License
ilz-calc
Copyright (C) 2020  Jonas Hucklenbroich <jonas@hucklenbroich.tk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.